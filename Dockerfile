FROM ubuntu:18.04

LABEL maintainer "anders.lindholsson@ess.eu"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    file \
    patch \
    tree \
    git \
    curl \
    m4 \
    autoconf \
    automake \
    libtool \
    git \
    tclx \
    graphviz \
    re2c \
    wget \
    sudo \
    python3 \
    coreutils \
    build-essential \
    libreadline-dev \
  && rm -rf /var/lib/apt/lists/*

USER ${USERNAME}
WORKDIR /home/${USERNAME}
